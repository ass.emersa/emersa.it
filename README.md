<center><h1>Website Associazione Emersa</h1></center>

<center> <img style='height: 200px;'src='http://web.emersa.it/wp-content/themes/emersa/img/logo.png'> </center>

Questo progetto contiene il client web per il sito emersa.it ora hostato su (assemersa.freemyip.com o web.emersa.it).

## Tecnologie/Framework/Librerie utilizzate

Il progetto è scritto con HTML, Javascript e CSS.

Per **gestire i formati delle date** viene utilizzato [momentjs](https://momentjs.com/).

Il **controllo versione** viene fatto con [Git](https://git-scm.com/).

Il **bundle del progetto** viene fatto con tramite [Webpack](https://webpack.js.org/). E' presente un file `package.json` che indica le dipendenze [NodeJS](https://nodejs.org/en/) utilizzate dal progetto, è necessario quindi avere installato anche NPM (disolito installato insieme a NodeJS).

#### Wordpress API
Questo sito ottiene le informazioni utilizzate nell'interfaccia grafica dall'installazione Wordpress di Emersa, per farlo utilizza la [WPAPI](http://wp-api.org/node-wpapi/).

## Impostare l'ambiente di sviluppo

Effettuare il clone della repo e scaricare le dipendenze:

```bash
git clone https://gitlab.com/ass.emersa/emersa.it.git
cd emersa.it
npm i
```

Impostare come variabile ambientale NODE_ENV:
```bash
export NODE_ENV='development'
```

Tramite:
```bash
npm run dev
```
Viene impostato un server accessibile da: `localhsot:8080`.

## Creare la build di produzione

Si imposta la variabile ambientale NODE_ENV in 'production':
```bash
export NODE_ENV='production'
```
Eseguire quindi:
```bash
npm run build
```
Per avere nella cartella `dist` la build pronta per essere hostata.

## Licenza

Al progetto viene applicata la [GNU Affero General Public License v3.0](LICENSE).
