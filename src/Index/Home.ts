import ScrollReveal from "scrollreveal";
const ENDPOINT = "http://assemersa.freemyip.com/?rest_route=/";
const WPAPI = require("wpapi");
const wp = new WPAPI({ endpoint: ENDPOINT });

/**
 *
 */
class Home {
  loader: HTMLElement;
  domMenuIcon: HTMLElement;
  domMenu: HTMLElement;
  domPosts: HTMLElement;

  constructor() {
    this.setInitScript();
  }

  createPost(title: string, date: string, content: string, url: string) {
    const template = document.createElement("template");

    template.innerHTML = `
    <div class="post">
            <div id="header">
                <div id="title">${title}</div>
                <div id="date">${date}</div>
            </div>
            <img src='${url}' >
            <div id="content">
                ${content}
            </div>
        </div>
    `;

    return template.content.cloneNode(true);
  }

  async downloadPosts() {
    const posts = (await wp.posts()).slice(0, 3);
    
    for (const post of posts) {
      if (post.featured_media) {
        const o = await wp.media().id(post.featured_media);
        this.domPosts.appendChild(
          this.createPost(
            post.title.rendered,
            new Date(post.date).toLocaleDateString(),
            post.content.rendered,
            o.source_url
          )
        );
      } else {
        const resp = await fetch(post._links["wp:attachment"][0].href)
        const json = await resp.json();
        if (json[0] && json[0].id) {
          const o = await wp.media().id(json[0].id);
          this.domPosts.appendChild(
            this.createPost(
              post.title.rendered,
              new Date(post.date).toLocaleDateString(),
              post.content.rendered,
              o.source_url
            )
          );
        }
      }
    }
  }

  setInitScript() {
    document.addEventListener("DOMContentLoaded", async () => {
      this.loader = document.querySelector("#background");
      this.domMenuIcon = document.querySelector("#menu-icon");
      this.domMenu = document.querySelector("#menu");
      this.domPosts = document.querySelector("#posts");

      await this.downloadPosts();

      this.domMenuIcon.addEventListener("click", () => {
        this.domMenu.classList.toggle("show");
      });

      ScrollReveal().reveal("#dove-title", {
        distance: "150%",
        origin: "left",
        opacity: 0.5
      });

      ScrollReveal().reveal(".writing", {
        distance: "150%",
        origin: "left",
        opacity: 0.5
      });

      ScrollReveal().reveal(".first-image", {
        distance: "150%",
        origin: "right",
        opacity: 0.5
      });

      ScrollReveal().reveal(".rap-image", {
        distance: "150%",
        origin: "right",
        opacity: 0.5
      });

      /*

      ScrollReveal().reveal("#locandina-phone", {
        distance: "150%",
        origin: "right",
        opacity: 0.5
      });

      ScrollReveal().reveal("#deriansky", {
        distance: "150%",
        origin: "top",
        opacity: 0.5
      });

      ScrollReveal().reveal("#deriansky", {
        distance: "150%",
        origin: "bottom",
        opacity: 0.5
      });

      ScrollReveal().reveal("#map", {
        distance: "150%",
        origin: "left",
        opacity: 0.5
      });
      
      */

      let delay: number = 5000;
      if (process.env.NODE_ENV == "development") {
        delay = 1000;
      }

      setTimeout(() => {
        this.loader.style.opacity = "0";
      }, delay);
      setTimeout(() => {
        this.loader.style.display = "none";
      }, delay + 1000);
    });
  }
}
export default Home;
