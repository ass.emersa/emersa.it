const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const StyleExtHtmlWebpackPlugin = require('style-ext-html-webpack-plugin');

module.exports = {
    mode: "development",
    entry: {
        main: './main.ts',
    },
    output: {
        filename: '[name].[hash].js',
        path: path.resolve(__dirname, 'dist'),
        chunkFilename: '[name].[hash].js',
    },
    plugins: [
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: './src/Index/index.html',
            inject: true,
        }),
        new MiniCssExtractPlugin({
            filename: '[name].css',
            chunkFilename: '[id].css',
            ignoreOrder: false, // Enable to remove warnings about conflicting order
        }),
        new ScriptExtHtmlWebpackPlugin({
            defaultAttribute: 'defer',
        }),

        new CleanWebpackPlugin(),
    ],
    devServer: {
        contentBase: './dist',
        hot: true,
    },
    optimization: {
        splitChunks: {
            chunks: 'all',
        },
    },
    devtool: 'inline-source-map',
    module: {
        rules: [
            {
                test: /\.html$/,
                exclude: /(index.html)/,
                loader: 'html-loader',
                options: {
                    interpolate: true,
                },
            },
            {
                exclude: /node_modules/,
                test: /\.ts?$/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: ['@babel/preset-env'],
                            plugins: ['@babel/plugin-transform-runtime'],
                        },
                    },
                    'ts-loader'
                ],
            },
            { // CSS
                test: /\.css$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader
                    },
                    {
                        loader: 'css-loader',
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            ident: 'postcss',
                            plugins: [
                                require('autoprefixer'),
                            ],
                        },
                    },
                ],
            },
            {
                test: /(head-logo.png)/,
                loader: 'url-loader',
                options: {
                    esModule: false
                }
            },
            { // Image files: png, svg, jpg, gif
                test: /\.(webp|png|svg|jpg|gif)$/,
                //exclude: /(logo.png|Copertina.png)/,
                loader: 'file-loader',
                options: {
                    esModule: false
                }
            },
        ],
    },
};